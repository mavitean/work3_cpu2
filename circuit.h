/**********************************************************************
* File: circuit.h
* Device: TMS320F2837xD
* Author: Marco Vinicio
* Description: Define inverter circuit parameter
**********************************************************************/

#ifndef CIRCUIT_H_
#define CIRCUIT_H_

#define CIR_R (float)2
#define CIR_L (float)0.01
#define CIR_E (float)48

#endif /* CIRCUIT_H_ */
