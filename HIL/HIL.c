/*****************************************************************************/
/**
 * @file HIL.c
 * @brief Source file of HIL
 *
 * Hardware in the loop 3-phase 2 level inverter RL load model
 *
 * @author Maco Vinicio (MVTA)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/

#include "HIL/HIL.h"
#include <stdint.h>

/*****************************************************************************/
/**
 * This function initializes the HIL instance, including all instances that it uses.
 *
 * @param   self is the HIL instance we are working on.
 *
 * @param   E is the dc bus voltage in volts.
 *
 * @param   R is the RL load resistance value in ohms.
 *
 * @param   L is the RL load inductor value in henry.
 *
 * @param   Ts is the calculation rate in seconds.
 *
 * @return  None.
 *
 * @note    None.
 *
 *****************************************************************************/
void HIL_init(HIL* self, const float E, const float R, const float L, const float Ts)
{
    self->E = E;
    self->R = R;
    self->L = L;

    self->ia = 0;
    self->ib = 0;
    self->ic = 0;

    self->Ts = Ts;

    self->commands = (float*) turn_off;
}

/*****************************************************************************/
/**
 * This function calculates the output according to the reference, feedback and
 * feedforward, if the controller is enabled. It should be called with the rate
 * specified by Ts.
 *
 * @param   Self is the instance we are working on.
 *
 * @return  None.
 *
 * @note    While disabled, the output is still calculated according to integrInit
 * and the feedforward.
 *
 *****************************************************************************/
#pragma CODE_SECTION(HIL_run, ".TI.ramfunc")
void HIL_run(HIL* self)
{
    //--- Copy duty cycle calculated by cpu1
    float duty[6];
    duty[0] = self->commands[0];
    duty[1] = self->commands[1];
    duty[2] = self->commands[2];
    duty[3] = self->commands[3];
    duty[4] = self->commands[4];
    duty[5] = self->commands[5];

    //--- Phases mean voltages
    float Va, Vb, Vc, Vn;
    Va = duty[0]*self->E/(float)2 - duty[1]*self->E/(float)2;
    Vb = duty[2]*self->E/(float)2 - duty[3]*self->E/(float)2;
    Vc = duty[4]*self->E/(float)2 - duty[5]*self->E/(float)2;

    //--- Virtual neutral mean voltage
    Vn = (Va + Vb+ Vc)/(float)3;

    //--- Phase current calculation
    self->ia = self->ia + (Va-Vn-self->R*self->ia)/self->L*self->Ts;
    self->ib = self->ib + (Vb-Vn-self->R*self->ib)/self->L*self->Ts;
    self->ic = self->ic + (Vc-Vn-self->R*self->ic)/self->L*self->Ts;
}

/*****************************************************************************/
/**
 * This function enables the HIL.
 *
 * @param   self is the instance we are working on.
 *
 * @return  None.
 *
 * @note    Enable Hil by connecting switching commands.
 *
 *****************************************************************************/
void HIL_enable(HIL* self, float* commands)
{
    self->commands = commands;
}

/*****************************************************************************/
/**
 * This function disables the HIL.
 *
 * @param   self is the instance we are working on.
 *
 * @return  None.
 *
 * @note    Disables switches commands.
 *
 *****************************************************************************/
void HIL_disable(HIL* self)
{
    self->commands = (float*)turn_off;
}

/*****************************************************************************/
/**
 * This function resets the HIL.
 *
 * @param   self is the instance we are working on.
 *
 * @return  None.
 *
 * @note    Disables switches commands and reset currents.
 *
 *****************************************************************************/
void HIL_reset(HIL* self)
{
    self->commands = (float*)turn_off;
    self->ia = 0;
    self->ib = 0;
    self->ic = 0;
}
