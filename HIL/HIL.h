/*****************************************************************************/
/**
 * @file HIL.h
 * @brief HIL header file.
 *
 * @author Marco Vinicio (MVTA)
 *
 * @copyright Universidade Federal de Minas Gerais. All rights reserved
 *****************************************************************************/

#ifndef HIL_HIL_H_
#define HIL_HIL_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct {
    // Circuit set parameters
    float E;
    float R;
    float L;

    // Circuit Currents
    float ia;
    float ib;
    float ic;

    // Model time step
    float Ts;

    // Switch control Inputs
    float* commands;

} HIL;

static const float turn_off[6] = {0, 0, 0, 0, 0, 0};

void HIL_run(HIL*);
void HIL_init(HIL*, const float, const float, const float, const float);
void HIL_enable(HIL*, float*);
void HIL_disable(HIL*);
void HIL_reset(HIL*);

#endif /* APPLICATION_CONTROL_PI_CONTROLLER_H_ */
