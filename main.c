//#############################################################################
//
// FILE:   empty_driverlib_main_cpu2.c
//
// TITLE:  Empty Project
//
// CPU2 Empty Project Example
//
// This example is an empty project setup for Driverlib development for CPU2.
//
//#############################################################################
// $TI Release: F2837xD Support Library v3.12.00.00 $
// $Release Date: Fri Feb 12 19:03:23 IST 2021 $
// $Copyright:
// Copyright (C) 2013-2021 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//#############################################################################

//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "math.h"

#include "HIL/HIL.h"
#include "circuit.h"

#include "F2837xD_device.h"

__interrupt void ipc0ISR(void);
__interrupt void epwm3ISR(void);

HIL model;

float Ia_buf[100];
float Ib_buf[100];
float Ic_buf[100];

uint16_t pos;

float i_alpha, i_beta;
uint16_t daca_value;
uint16_t dacb_value;


//
// Main
//
void main(void)
{
    //
    // Initialize device clock and peripherals.
    //
    Device_init();

    //--- CPU Initialization
    Interrupt_initModule();             // Initializes PIE, clear PIE registers, disables CPU interrupts, and clear all CPU interrupt flags (FILE: interrupt.c)
    Interrupt_initVectorTable();        // Initialize the PIE vector table with pointers to the shell interrupt Service Routines (FILE: interrupt.c)

    //---- Initialize HIL model
    HIL_init(&model, (float)48, (float)2, (float)0.01, (float)0.0002);

    //--- Initialize buffer
    pos = 0;

    //--- Clear IPC0 Flag
    IpcRegs.IPCCLR.bit.IPC0 = 1;
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);

    //--- Enable IPC0 PIE interrupt
    Interrupt_register(INT_IPC_0, &ipc0ISR);     // Re-map IPC1 interrupt signal to call the ISR function
    Interrupt_enable(INT_IPC_0);                 // Enable IPC1 in PIE group 1 and enable INT1 in IER to enable PIE group 1

    //--- Enable IPC0 PIE interrupt
    Interrupt_register(INT_EPWM3, &epwm3ISR);     // Re-map IPC1 interrupt signal to call the ISR function
    Interrupt_enable(INT_EPWM3);                 // Enable IPC1 in PIE group 1 and enable INT1 in IER to enable PIE group 1

    //--- Let CPU1 know that CPU2 is ready
    IpcRegs.IPCSET.bit.IPC17 = 1;       // Set IPC17 to release CPU1

    //--- Enable global interrupts and real-time debug
    EINT;                               // Enable global interrupts - defined as: asm(" clrc INTM"); (FILE: cpu.h)
    ERTM;                               // Enable real-time debug - defined as: asm(" clrc DBGM"); (FILE: cpu.h)

    //--- Main Loop
    while(1)                            // endless loop - wait for an interrupt
    {
        NOP;                            // No operation - defined as: asm(" NOP"); (FILE: cpu.h)
    }
}
//end of main()

__interrupt void ipc0ISR(void)
{
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);      // Must acknowledge the PIE group

    //--- Manage the IPC registers
    IpcRegs.IPCACK.bit.IPC0 = 1;                    // Clear IPC1 flag

    //--- Read the ADC result from IPC1
    HIL_enable(&model, (float*) IpcRegs.IPCRECVADDR);
}

__interrupt void epwm3ISR(void)
{
    EPWM_clearEventTriggerInterruptFlag(EPWM3_BASE); // Clear INT flag for this timer
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);// Must acknowledge the PIE group

    //--- Run HIL to calculate inverter currents
    HIL_run(&model);

    //--- buffer for visualization purposes;
    Ia_buf[pos] = model.ia;
    Ib_buf[pos] = model.ib;
    Ic_buf[pos] = model.ic;

    pos = pos + 1;
    if(pos == 100){
        pos = 0;
    }

    //--- abc to Alpha Beta transform
    i_alpha = (model.ia - model.ib/(float)2 - model.ic/(float)2)*(float)2/(float)3;
    i_beta = (model.ib*(float)sqrt(3)/(float)2 - model.ic*(float)sqrt(3)/(float)2)*(float)2/(float)3;

    //--- update DACA and DACB values
    daca_value = (uint16_t) roundf((i_alpha+10)/(float)20*4096);
    dacb_value = (uint16_t) roundf((i_beta+10)/(float)20*4096);
    EALLOW;
    DacaRegs.DACVALS.all = daca_value;
    DacbRegs.DACVALS.all = dacb_value;
    EDIS;
}

//
// End of File
//
